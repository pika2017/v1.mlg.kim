// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import axios from './router/domain.js'
// import { wxConfig } from 'axios';
import Filter from './filter'
import { Base64 } from 'js-base64'
import Cookies from '../static/lib/cookie'

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requireAuth)) { // 判断该路由是否需要登录权限
        if (Vue.prototype.is_login) { // 判断当前的token是否存在
            next()
        } else {
            window.location.href = Vue.prototype.mlg_domain + 'User/Login' + '?redirect=' + Base64.encode(window.location.href)
        }
    } else {
        next()
    }
})

// Vue.use(wx);
Vue.use(Filter)

Vue.prototype.$axios = axios
// Vue.prototype.router = router
Vue.config.productionTip = false

Vue.prototype.domain = 'http://dev.mlg.kim/Api/V1/'
Vue.prototype.mlg_domain = 'http://dev.mlg.kim/'
Vue.prototype.is_login = Cookies.get('uid') ? 1 : 0

// if (!Vue.prototype.is_login && window.location.href.indexOf('/submit') === -1 && window.location.href) {
//     window.location.href = Vue.prototype.mlg_domain + 'User/Login' + '?redirect=' + encodeURIComponent(window.location.href)
// }

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App />'
})
