import axios from 'axios'

axios.defaults.baseURL = 'http://dev.mlg.kim/Api/V1/'

axios.defaults.headers.crossDomain = true

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

axios.defaults.withCredentials = true

export default axios

export const domain = 'http://dev.mlg.kim/Api/V1/'
