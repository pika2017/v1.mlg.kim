import Vue from 'vue'
import Router from 'vue-router'
import HomeMobile from '@/components/home_mobile/home_mobile.vue'
import Submit from '@/components/agency/submit/submit.vue'
import Board from '@/components/agency/board/board.vue'
import Detail from '@/components/agency/detail/detail.vue'
import Info from '@/components/agency/info/info.vue'
import List from '@/components/agency/list/list.vue'
import Response from '@/components/agency/response/response.vue'
import ResponseAgency from '@/components/agency/response_agency/response_agency.vue'
import ResponseSubmit from '@/components/agency/response_submit/response_submit.vue'
import Talking from '@/components/agency/talking/talking.vue'
Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'homeMobile',
            component: HomeMobile
        },
        {
            path: '/agency/response/:id',
            name: 'response',
            component: Response,
            meta: { requireAuth: true }
        },
        {
            path: '/agency/response_agency/:id',
            name: 'response_agency',
            component: ResponseAgency,
            meta: { requireAuth: true }
        },
        {
            path: '/agency/response_submit/:id',
            name: 'response_submit',
            component: ResponseSubmit,
            meta: { requireAuth: true }
        },
        {
            path: '/agency/submit_response/:id',
            name: 'detail',
            component: Detail,
            meta: { requireAuth: true }
        },
        {
            path: '/agency/info/:id',
            name: 'info',
            component: Info,
            meta: { requireAuth: true }
        },
        {
            path: '/agency/board',
            name: 'board',
            component: Board,
            meta: { requireAuth: true }
        },
        {
            path: '/agency/submit/:type/:id?',
            name: 'submit',
            component: Submit
        },
        {
            path: '/agency/list',
            name: 'list',
            component: List,
            meta: { requireAuth: true }
        },
        {
            path: '/agency/talking/:id?',
            name: 'talking',
            component: Talking,
            meta: { requireAuth: true }
        }
    ]
})
