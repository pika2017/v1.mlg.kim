import Swiper from '../../../static/lib/swiper/swiper.min.js'

export default {
    data () {
        return {
            indexPage: '1',
            needPage: '0',
            colorPage: '0',
            nimePage: '0',
            name: 11
        }
    },
    methods: {
        indexPageFn:function () {
            this.indexPage = '1';
            this.needPage='0';
            this.colorPage='0';
            this.nimePage='0';

        },
        needPageFn:function () {
            this.indexPage = '0';
            this.needPage='1';
            this.colorPage='0';
            this.nimePage='0';

        },
        colorPageFn:function () {
            this.indexPage = '0';
            this.needPage='0';
            this.colorPage='1';
            this.nimePage='0';

        },
        nimePageFn:function () {
            this.indexPage = '0';
            this.needPage='0';
            this.colorPage='0';
            this.nimePage='1';

        },
        swiper () {
            new Swiper('.swiper-container', {
                autoplay: 1000,
                loop: true,
                observer: true
            })
        }
    },
    created () {
        console.log('created2')
    },
    mounted () {
        this.swiper()
    }
}
