import plupload from '../../../../static/lib/plupload/js/plupload.min.js'
export default {
    data() {
        return {
            indexPage: '1',
            needPage: '0',
            colorPage: '0',
            nimePage: '0',
            results: '反馈一: ',
            content: [
                { name: '' },
                { add: '' },
                { phone: '' },
                { price: '' },
                { goods: '' },
            ],
            imageList: [],
            responseList:[],
        }
    },
    methods: {
        // 上传反馈图片
        loadImg: function (imgs) {
            imgs += 1;
            this.imgs = imgs;
            this.upPIc = Number(this.upPIc) + Number(1);
            let that = this;
            let uploader = new plupload.Uploader({
                browse_button: 'upload',
                url: 'http://dev.mlg.kim/Api/V1/upload_find_agency_image',
                flash_swf_url: '/src/js/plupload/Moxie.swf',
                silverlight_xap_url: 'js/Moxie.xap',
                filters: {
                    max_file_size: '10mb',
                    mime_types: [
                        { title: 'Image files', extensions: 'jpg,gif,png' },
                        { title: 'Zip files', extensions: 'zip' }
                    ]
                },
                init: {
                    FilesAdded: function (uploader, files) {
                        uploader.start();
                    }
                }
            });
            uploader.init();
            uploader.bind('FileUploaded', function (uploader, file, res) {
                function jsonpReturn(param) {
                    return param;
                }
                let data = eval(res.response);
                let imageData = data.data;
                that.imageList.push(imageData);
            })
        },
        indexPageFn: function () {
            this.indexPage = '1'
            this.needPage = '0'
            this.colorPage = '0'
            this.nimePage = '0'
        },
        needPageFn: function () {
            this.indexPage = '0'
            this.needPage = '1'
            this.colorPage = '0'
            this.nimePage = '0'
        },
        colorPageFn: function () {
            this.indexPage = '0'
            this.needPage = '0'
            this.colorPage = '1'
            this.nimePage = '0'
        },
        nimePageFn: function () {
            this.indexPage = '0'
            this.needPage = '0'
            this.colorPage = '0'
            this.nimePage = '1'
        },
        goBack: function () {
            this.$router.go(-1)
        },
        // 版哥提交反馈
        submitAgencyResponse: function () {
            let id = this.$route.params.id
            // console.log(id);
            this.content.push(this.imageList);
            console.log(this.content);
            this.$axios.get('submit_agency_response', {
              params:{
                id: id,
                content: this.content
              }
            }).then(res => {
                //  console.log(res);
                function jsonpReturn(params) {
                    return params;
                }
                var data = eval(res.data);
                console.log(data);
                if (data.code == 200) {
                    // this.$router.push({ name: "board", params: { id: id } });
                    this.getResponseList();
                }
            })
        },

        //获取反馈列表
        getResponseList() {
            let id = this.$route.params.id
            this.$axios.get('get_my_response_list', {
                params:{
                    id:id
                }
            }).then(res => {
                //  console.log(res);
                function jsonpReturn(params) {
                    return params;
                }
                var data = eval(res.data);
                // console.log(data);
                if (data.code == 200) {
                    //   this.responseList=
                     this.responseList=data.data;
                     for(var i=0;i<this.responseList.length;i++){
                           this.responseList[i].content=JSON.parse( this.responseList[i].content);
                           for(var j=0;j<this.responseList[i].content.length;j++){
                               this.responseList[i].content[j]=JSON.parse(this.responseList[i].content[j]);
                           }
                     }
                     console.log(this.responseList);
                }
            })
        }
    },
    created: function () {
        this.getResponseList();
    },
    mounted: function () {
        this.loadImg();
    }

}
