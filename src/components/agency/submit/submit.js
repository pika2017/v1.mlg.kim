import plupload from '../../../../static/lib/plupload/js/plupload.min.js'
import { Base64 } from 'js-base64'

export default {
    data() {
        return {
            uploadProcess: 0,
            typeName: {
                fabric: {
                    name: "面料"
                },
                subfabric: {
                    name: "辅料"
                }
            },
            curType: 'fabric',
            classifyList: {
                fabric: [
                    {
                        id: '1',
                        name: '针织'
                    },
                    {
                        id: '2',
                        name: '棉布'
                    },
                    {
                        id: '3',
                        name: '针织'
                    },
                    {
                        id: 4,
                        name: '棉布'
                    }
                ],
                subfabric: [
                    {
                        id: '1',
                        name: '纽扣'
                    },
                    {
                        id: '2',
                        name: '花边'
                    },
                    {
                        id: '3',
                        name: '色带'
                    }
                ]
            },
            classifySelected: 1,

            money: this.$route.params.type == 1 ? 50 : 20,
            mengData: false,
            id: 1,
            upPIc: 0,
            imgaPath: '../../../res/submit/pic2.png',
            imageList: [],
            imgs: 1,
            indexPage: '1',
            needPage: '0',
            colorPage: '0',
            nimePage: '0',
            choiceColorA: true,
            choiceColorB: false,
            choiceColorC: true,
            choiceColorD: false,
            des: '',
            classifier: '个',
            classifiers: ['个', '只'],
            hours: [
                // {name:'请选择',num: '0', money1: 0, money2: 0 },
                { name: '3小时', num: '3', money1: 50, money2: 20 },
                { name: '6小时', num: '6', money1: 40, money2: 15 },
                { name: '24小时', num: '12', money1: 30, money2: 10 },
                { name: '3天', num: '72', money1: 20, money2: 5 }],
            // hour:  {name:'请选择',num: '0', money1: 0, money2: 0 },
            hour: { name: '3小时', num: '3', money1: 50, money2: 20 },

            describe: '用来夏天做衣服的布料',
            numberGoods: 0,
            order: '不接受',
            futures: '现货',
            timeToFind: '1',
            phone: '',
            avaNow: '1',
            sim: '1',
            description: '',
            type: '',
            currentUrl: ''
        }
    },
    watch: {
        choiceColorA: function () {
            this.choiceColorA ? this.sim = '1' : this.sim = '2'
        },
        choiceColorC: function () {
            this.choiceColorC ? this.avaNow = '1' : this.avaNow = '2'
        },
        hour: function () {

            var type = this.$route.params.type;

            if (type == 1) {
                this.money = this.hour.money1;
            } else if (type == 2) {
                this.money = this.hour.money2;
            }
        }
    },
    methods: {

        changeMengData() {
            this.mengData = !this.mengData;
        },
        closeMeng() {
            this.mengData = !this.mengData;
        },
        goBack: function () {
            this.$router.go(-1)
        },
        numberGoodsAdd: function () {
            this.numberGoods = this.numberGoods + 1
        },
        numberGoodsSub: function () {
            this.numberGoods == 0 ? this.numberGoods = 0 : this.numberGoods = this.numberGoods - 1
        },
        olderAccept: function () {
            this.choiceColorA = !this.choiceColorA;
            this.choiceColorB = !this.choiceColorB;
        },
        olderUnAccept: function () {
            this.choiceColorA = !this.choiceColorA;
            this.choiceColorB = !this.choiceColorB;
        },
        nowGoods: function () {
            this.choiceColorC = !this.choiceColorC;
        },
        featureGoods: function () {
            this.choiceColorD = !this.choiceColorD;
        },
        loadImg: function (imgs) {
            imgs += 1;
            this.imgs = imgs;
            this.upPIc = Number(this.upPIc) + Number(1);
            let that = this;
            let uploader = new plupload.Uploader({
                browse_button: 'upload',
                url: that.mlg_domain + 'Api/V1/upload_find_agency_image',
                flash_swf_url: '/static/lib/plupload/js/Moxie.swf',
                silverlight_xap_url: '/static/lib/plupload/js/Moxie.xap',
                filters: {
                    max_file_size: '10mb',
                    mime_types: [
                        {title: 'Image files', extensions: 'jpg,gif,png,jpeg'}
                    ]
                },
                init: {
                    FilesAdded: function (uploader, files) {
                        that.uploadProcess = 0
                        uploader.start();
                    }
                }
            });
            uploader.init();
            uploader.bind('FileUploaded', function (uploader, file, res) {
                function jsonpReturn(param) {
                    return param;
                }

                that.uploadProcess = 0
                let data = eval(res.response)
                let imageData = data.data
                that.$data.imageList.push(imageData)
            })

            uploader.bind('UploadProgress', function (uploader, file) {
                let percent = file.percent
                that.uploadProcess = percent
            })
        },
        submitFn: function () {
            this.$axios.get('submit_agency_apply', {
                params: {
                    id: this.$route.params.id,
                    des: this.description,
                    images: this.imageList,
                    number: this.numberGoods,
                    similar: this.sim,
                    available: this.avaNow,
                    hour: this.hour.num,
                    phone: this.phone,
                    sub_type: this.classifySelected,
                    type: this.$route.params.type
                }
            }).then(res => {
                function jsonpReturn(param) {
                    return param;
                }
                var data = eval(res.data);

                if (data.code == "200") {
                    this.$router.push({ name: 'list' })
                } else if (data.code == "201") {
                    window.location.href = "http://dev.mlg.kim/pay/order/" + data.data.order_id;
                } else {
                    alert(data.message);
                }
            })
        },
        // 修改提交
        editSumbit: function () {

        },
        // 获取版代客找版内容详情
        getDetailContent(id) {
            this.$axios.get("get_agency_detail", {
                params: {
                    id: id
                }
            }).then(res => {

                function jsonpReturn(param) {
                    return param;
                }

                let data = eval(res.data);

                if (data.code == 200) {

                    data.data.image = JSON.parse(data.data.image);

                    if (data.data.image) {
                        for (var j = 0; j < data.data.image.length; j++) {
                            data.data.image[j] = JSON.parse(data.data.image[j]);
                        }
                    }

                    this.description = data.data.description;
                    this.imageList = data.data.image;
                    this.numberGoods = data.data.number;
                    this.sim = data.data.similar;
                    this.avaNow = data.data.available;
                    this.hour.num = data.data.time_limit;
                    this.phone = data.data.phone;
                    this.type = data.data.type;
                    this.classifySelected = data.data.sub_type
                }
            })
        }
    },
    created: function () {
        this.id = this.$route.params.id
        this.type = this.$route.params.type

        if (this.type == 1) {
            this.curType = 'fabric'
        } else {
            this.curType = 'subfabric'
        }

        if (this.id) {
            this.getDetailContent(this.id)
        }
    },
    mounted: function () {
        this.loadImg()
        this.currentUrl = Base64.encode(window.location.href)
    }
}
