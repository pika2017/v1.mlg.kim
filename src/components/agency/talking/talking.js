import plupload from '../../../../static/lib/plupload/js/plupload.min.js'
export default {
    data() {
        return {
            talkDetal: 'jljsdsdfsasfsdfasdfasdfsdffffl',
            indexPage: '1',
            needPage: '0',
            colorPage: '0',
            nimePage: '0',
            time: '10:14',
            text: 'sfjlsdjsdfffffffffffffffffff',
            message: '',
            // id: this.getCookie("uid"),
            historyMessage: [],
            showSendImg: {},
            uid: this.$route.params.id,
        }
    },
    watch: {
        historyMessage() {
            this.$nextTick(() => {
                this.scrollToBottom();
            });
        },
    },
    methods: {
        //上传图片
        sendImg: function (imgs) {
            imgs += 1;
            this.imgs = imgs;
            this.upPIc = Number(this.upPIc) + Number(1);
            let that = this;
            let uploader = new plupload.Uploader({
                browse_button: 'upload',
                url: 'http://dev.mlg.kim/Api/V1/upload_find_agency_image',
                flash_swf_url: '/src/js/plupload/Moxie.swf',
                silverlight_xap_url: 'js/Moxie.xap',
                filters: {
                    max_file_size: '10mb',
                    mime_types: [
                        { title: 'Image files', extensions: 'jpg,gif,png' },
                        { title: 'Zip files', extensions: 'zip' }
                    ]
                },
                init: {
                    FilesAdded: function (uploader, files) {
                        uploader.start();
                    }
                }
            });
            uploader.init();

            uploader.bind('FileUploaded', function (uploader, file, res) {
                function jsonpReturn(param) {
                    return param;
                }
                let data = eval(res.response);
                console.log(data);
                this.showSendImg = data.data;
                console.log(this.showSendImg);

                that.$axios.get("http://dev.mlg.kim/Api/V1/send_message", {
                    params: {
                        message: this.showSendImg,
                        type: 1,
                        to_uid: that.uid
                    }
                }).then(res => {
                    function jsonpReturn(param) {
                        return param;
                    }
                    console.log(res);
                    var data = eval(res.data);
                    console.log(data);
                    if (data.code == 200) {
                        // this.getNewmessage();
                        that.getTalkHistory();
                    }
                })
            })
        },
        // // 合并两个数组去除重复项
        // concat_:function(arr1, arr2) {
        //     //不要直接使用var arr = arr1，这样arr只是arr1的一个引用，两者的修改会互相影响  
        //     var arr = arr1.concat();
        //     //或者使用slice()复制，var arr = arr1.slice(0)  
        //     for (var i = 0; i < arr2.length; i++) {
        //         arr.indexOf(arr2[i]) === -1 ? arr.push(arr2[i]) : 0;
        //     }
        //     return arr;
        // },
        // 去除数组中重复的对象
        deleteRepeat: function (arr) {
            var unique = {};
            arr.forEach(function (a) { unique[JSON.stringify(a)] = 1 });
            arr = Object.keys(unique).map(function (u) { return JSON.parse(u) });
            return arr
        },
        // 设置显示时间
        showTime: function (time) {
            var date1 = new Date();  //现在时间
            var date2 = new Date(time);    //传入时间
            var date3 = date1.getDate() - date2.getDate()  //时间差的毫秒数
            //计算出相差天数
            // var days = Math.floor(date3 / (24 * 3600 * 1000));
            // console.log(days);
            if (date3 >= 1) {
                return date2.getMonth() + 1 + "月" + date2.getDate() + "日"
            } else {
                var hours = date2.getHours() > 10 ? date2.getHours() : "0" + date2.getHours();
                var minutes = date2.getMinutes() > 10 ? date2.getMinutes() : "0" + date2.getMinutes();
                return hours + ":" + minutes
                // return  date2.getHours()  + ":" + date2.getMinutes() 
            }
            //计算出相差天数
            var days = Math.floor(date3 / (24 * 3600 * 1000))
        },
        // 设置滚动条在底部
        scrollToBottom: function () {
            var scollBox = document.querySelectorAll("#scollBox")[0]
            var item_content = document.querySelectorAll("#item_content")[0]
            console.log(item_content.clientHeight)
            scollBox.scrollTop = item_content.clientHeight
        },
        // 获取cookie值uid
        getCookie(cookieName) {
            console.log(cookieName);
            var strCookie = document.cookie;
            var arrCookie = strCookie.split("; ");
            console.log(arrCookie);
            for (var i = 0; i < arrCookie.length; i++) {
                var arr = arrCookie[i].split("=");
                if (cookieName == arr[0]) {
                    return arr[1];
                }
            }
            return "";
        },
        // 获取聊天记录
        getTalkHistory() {
            console.log(this.uid);
            this.$axios.get('get_message_history', {
                headers: {
                    'crossDomain': true
                },
                params: {
                    id: this.uid
                }
            }).then(res => {
                function jsonpReturn(param) {
                    return param;
                }
                console.log(res);
                var data = eval(res.data);
                console.log(data);
                if (data.code == 200) {
                    this.historyMessage = data.data.reverse();
                    for (var i = 0; i < this.historyMessage.length; i++) {
                        this.historyMessage[i].ctime = this.showTime(this.historyMessage[i].ctime);
                        if (this.historyMessage[i].type == 1) {
                            if (this.historyMessage[i].message) {
                                this.historyMessage[i].message = JSON.parse(this.historyMessage[i].message);
                            }
                        }

                    }
                }
            })
        },
        //获取新信息
        getNewmessage() {
            // var uid = this.getCookie("uid");
            // console.log(uid);
            this.$axios.get("get_new_message", {
                params: {
                    from_uid: this.uid
                }
            }).then(res => {
                function jsonpReturn(param) {
                    return param;
                }
                var data = eval(res.data);
                console.log(data.data);
                if (data.code == 200) {
                    if (data.data) {
                        for (var i = 0; i < data.data.length; i++) {
                            data.data[i].ctime = this.showTime(data.data[i].ctime);
                            if (data.data[i].type == 1) {
                                if (data.data[i].message) {
                                    data.data[i].message = JSON.parse(data.data[i].message);
                                }
                            }
                        }
                        this.historyMessage = this.historyMessage.concat(data.data);
                        this.historyMessage = this.deleteRepeat(this.historyMessage)
                        console.log(this.historyMessage);
                        // for (var i = 0; i < this.historyMessage.length; i++) {
                        //     this.historyMessage[i].ctime = this.showTime(this.historyMessage[i].ctime);
                        //     if (this.historyMessage[i].type == 1) {
                        //         if (this.historyMessage[i].message) {
                        //             this.historyMessage[i].message = JSON.parse(this.historyMessage[i].message);
                        //         }
                        //     }

                        // }
                    }
                }
            })
        },
        //发送信息
        sendMessage() {
            var uid = this.$route.params.id;
            // console.log(uid);
            this.$axios.get("send_message", {
                params: {
                    message: this.message,
                    type: 2,
                    to_uid: this.uid
                }
            }).then(res => {
                function jsonpReturn(param) {
                    return param;
                }
                // console.log(res);
                var data = eval(res.data);
                // console.log(data);
                if (data.code == 200) {
                    this.getTalkHistory();
                    this.message = "";
                }
            })

        },
        indexPageFn: function () {
            this.indexPage = '1';
            this.needPage = '0';
            this.colorPage = '0';
            this.nimePage = '0';

        },
        needPageFn: function () {
            this.indexPage = '0';
            this.needPage = '1';
            this.colorPage = '0';
            this.nimePage = '0';

        },
        colorPageFn: function () {
            this.indexPage = '0';
            this.needPage = '0';
            this.colorPage = '1';
            this.nimePage = '0';

        },
        nimePageFn: function () {
            this.indexPage = '0';
            this.needPage = '0';
            this.colorPage = '0';
            this.nimePage = '1';

        },
        goBack: function () {
            this.$router.go(-1)
        }
    },


    created: function () {
        this.getTalkHistory();
    },

    mounted: function () {
        this.sendImg();
        window.timerId = setInterval(() => {
            this.getNewmessage();
            console.log(this.timer);
        }, 10000);
        console.log(this.timer);
    },

    destroyed: function () {
        clearInterval(window.timerId);
    }
}