
export default {
    data () {
        return {
            datas:[],
            content:[
                {name:''},
                {add:''},
                {phone:''},
                {price:''},
                {goods:''}
            ],
            responseList:[],
            applyId: '',
            isShow:false
        }
    },
    methods: {
        goBack: function () {
            this.$router.go(-1)
        },
        getApplyDetail () {

            this.$axios.get('get_agency_detail', {
                params: {
                    id: this.applyId
                }
            }).then(res => {
                function jsonpReturn(datas) {
                    return datas
                }

                this.globalData = eval(res.data)
            })
        },
         // 获取反馈列表
        getData: function () {
            var id = this.$route.params.id;
            this.$axios.get('get_agency_response_list', {
                headers: {
                    'crossDomain': true
                },
                params: {
                    id:this.$route.params.id
                }
            }).then(res => {
                console.log(res)
                function jsonpReturn(param) {
                    return param;
                }
                let data = eval(res.data);
                if(data.code==200){
                    this.responseList=data.data;
                    for(var i=0;i<this.responseList.length;i++){
                          this.responseList[i].content=JSON.parse( this.responseList[i].content);
                          for(var j=0;j<this.responseList[i].content.length;j++){
                              this.responseList[i].content[j]=JSON.parse(this.responseList[i].content[j]);
                          }

                          this.agencyUid = this.responseList[i].agency_user_id
                    }
                    console.log(this.responseList);
                }

            })
        },

        contactAgency: function() {
            this.$router.push({ name: 'talking', params: { id: this.agencyUid } })
        },
        // 结单
        changeApplyStatus () {
            this.$axios.get('finish_apply', {
                params: {
                    id: this.applyId
                }
            }).then(res => {

                function jsonpReturn(param) {
                    return param;
                }

                let data = eval(res.data);
                if (data.code === "200") {
                    alert('结单成功');
                    window.location.reload()
                } else {
                    alert(data.message)
                }
            })
        },

        // 确认、反馈版哥反馈
        confirmAgencyResponse: function (id,status) {

            this.$axios.get('confirm_agency_response', {
                headers: {
                    'crossDomain': true
                },
                params: {
                    status: status,
                    id: id
                }
            }).then(res => {
                function jsonpReturn(param) {
                    return param;
                }
                let data = eval(res.data);

                if(data.code == '200'){
                    window.location.reload()
                }else{
                    alert(data.message);
                }
            })
        }
    },
    created: function () {

        this.applyId = this.$route.params.id
        this.getData()
        this.getApplyDetail()
    }
}
