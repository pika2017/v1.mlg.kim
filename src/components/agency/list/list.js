export default {
    data () {
        return {
            feedback: '1',
            indexPage: '1',
            needPage: '0',
            colorPage: '0',
            nimePage: '0',
            iconfont: '1',
            seek: '0',
            finsh: '0',
            sentTime: '已经',
            cancel: '取消',
            edit: '取消',
            contact: '编辑',
            datas:[],
            data:[],
            flag: 1,
            imgs:[1,2,3],
            classifyList: {
                fabric: [
                    {
                        id: '1',
                        name: '针织'
                    },
                    {
                        id: '2',
                        name: '棉布'
                    },
                    {
                        id: '3',
                        name: '针织'
                    },
                    {
                        id: 4,
                        name: '棉布'
                    }
                ],
                subfabric: [
                    {
                        id: '1',
                        name: '纽扣'
                    },
                    {
                        id: '2',
                        name: '花边'
                    },
                    {
                        id: '3',
                        name: '色带'
                    }
                ]
            }
        }
    },
    methods: {
        // 客户需求等样式控制函数
        changeContainFn: function (cc) {
            if (cc == '1' ) {
                this.getData('1','1');
                this.feedback = '1';
                this.seek = '0';
                this.finsh = '0'
            } else if (cc == '2') {
                this.getData('1','2');
                this.feedback = '0';
                this.seek = '1';
                this.finsh = '0'
            } else if (cc == '3') {
                this.getData('1','3');
                this.feedback = '0';
                this.seek = '0';
                this.finsh = '1'
            }
        },
        // 接单
        receivingOrder: function (id) {
            console.log(id);
            this.$axios.get('accept_apply', {
                params: {
                    id: id
                }
            }).then(res => {
                function jsonpReturn (param) {
                    return param
                }
                let data = eval(res.data);
                if(data.code == "200"){
                    alert(data.message);
                    this.changeContainFn(2);
                } else {
                    alert(data.message)
                }
            })
        },


        // 页面列表接口，可以判断类型
        getData: function (type, status) {
            if (!this.flag) {
                return
            }
            this.flag = false
            this.datas = {}

            this.$axios.get('get_apply_list', {
                params: {
                    type: type,
                    status: status
                }
            }).then(res => {
                function jsonpReturn (param) {
                    return param
                }

                let datas = eval(res.data)
                this.datas = datas.data
                this.flag = true

                if (datas.code === '200') {
                    for (let i = 0; i < this.datas.length; i++) {
                        this.datas[i].image = JSON.parse(this.datas[i].image)

                        if (this.datas[i].type === '1') {
                            for (let index in this.classifyList['fabric']) {
                                if (this.datas[i].sub_type === this.classifyList['fabric'][index]['id']) {
                                    this.datas[i].type_name = '面料 - ' + this.classifyList['fabric'][index]['name']
                                }
                            }
                        } else if (this.datas[i].type === '2') {
                            for (let index in this.classifyList['subfabric']) {
                                if (this.datas[i].sub_type === this.classifyList['subfabric'][index]['id']) {
                                    this.datas[i].type_name = '辅料 - ' + this.classifyList['subfabric'][index]['name']
                                }
                            }
                        }

                        if (!this.datas[i].type_name) {
                            this.datas[i].type_name = '类别未知'
                        }

                        if (this.datas[i].image) {
                            for (let j = 0; j < this.datas[i].image.length; j++) {
                                this.datas[i].image[j] = JSON.parse(this.datas[i].image[j])
                                if (this.datas[i].image[j]['path'] && !this.datas[i].image_single) {
                                    this.datas[i].image_single = this.datas[i].image[j]['domain'] + this.datas[i].image[j]['path']
                                } else {
                                    this.datas[i].image_single = ''
                                }
                            }
                        }

                        // 超时标志
                        if (new Date(this.datas[i].ctime.replace("/-/g", '/')).getTime() + this.datas[i].time_limit * 3600 * 1000 < new Date().getTime()) {
                            this.datas[i].overtime = 1
                        }

                        // 最晚反馈时间
                        let latestTime = new Date(this.datas[i].ctime.replace("/-/g", '/')).getTime() + parseInt(this.datas[i].time_limit) * 3600 * 1000
                        this.datas[i].latest_time = new Date(latestTime).getMonth() + 1 + '-' +
                            new Date(latestTime).getDate() + ' ' +
                            new Date(latestTime).getHours() + ':' +
                            new Date(latestTime).getMinutes()
                    }
                }
            })
        },

        routerLinkDetail (id) {
            this.$router.push({ name: 'info', params: { id: id } })
        },
        // 转单
        cancelAcceptedApply: function (id) {
            this.$axios.get('cancel_accepted_apply', {
                params: {
                    id: id
                }
            }).then(res => {
                function jsonpReturn (param) {
                    return param
                }

                let data= eval(res.data)

                if (data.code == 200) {
                    alert(data.message)
                }else{
                    alert(data.message)
                }
            })
        }
    },
    // 默认拿到客户需求数据
    created: function () {
        this.getData('1','1')
    }
}
