export default {
    data() {
        return {
            // uid:'',
            indexPage: '1',
            needPage: '0',
            colorPage: '0',
            nimePage: '0',
            buyNumber: '12',
            order: '',
            futures: '',
            globalData: '',
            timeToFind: '1',
            phone: '110',
            routerId: '2',
            id: '1',
            data: {},
            typeName: "加载中...",
            applyTime: "加载中...",
            classifyList: {
                fabric: [
                    {
                        id: '1',
                        name: '针织'
                    },
                    {
                        id: '2',
                        name: '棉布'
                    },
                    {
                        id: '3',
                        name: '针织'
                    },
                    {
                        id: 4,
                        name: '棉布'
                    }
                ],
                subfabric: [
                    {
                        id: '1',
                        name: '纽扣'
                    },
                    {
                        id: '2',
                        name: '花边'
                    },
                    {
                        id: '3',
                        name: '色带'
                    }
                ]
            }
        }
    },
    methods: {
        goBack: function () {
            this.$router.go(-1)
        },
        changeApply () {
            this.$router.push('/agency/submit/' + this.data.type + '/' + this.data.id)
        },
        viewAllApply () {
            this.$router.push('/agency/board')
        },
        // 接单
        receivingOrder: function (id) {
            console.log(id);
            this.$axios.get('accept_apply', {
                params: {
                    id: id
                }
            }).then(res => {
                function jsonpReturn (param) {
                return param
            }
            var data = eval(res.data);
            if(data.code == "200"){
                alert(data.message);
                this.changeContainFn(2);
            }
        })
        },
        submitResponse () {
            this.$router.push({name:'response_submit',params:{id: this.data.id}})
        },
        contactCustomer: function() {
            this.$router.push({ name: 'talking', params: { id: this.data.uid } })
        },
        // 转单
        cancelAcceptedApply: function (id) {
            this.$axios.get('cancel_accepted_apply', {
                params: {
                    id: id
                }
            }).then(res => {
                function jsonpReturn (param) {
                return param
            }

            let data= eval(res.data)

            if (data.code == 200) {
                alert(data.message)
            }else{
                alert(data.message)
            }
        })
        },
        // 获取数据
        getData: function () {
            this.id = this.$route.params.id
            this.$axios.get('get_agency_detail', {
                params: {
                    id: this.id
                }
            }).then(res => {
                function jsonpReturn(datas) {
                    return datas
                }

                this.globalData = eval(res.data)
                this.data = eval(res.data).apply.data;
                this.data.is_available === 1 ? this.data.is_available = '现货' : this.data.is_available = '期货'
                this.data.is_similar === 1 ? this.data.is_similar = '接受' : this.data.is_similar = '不接受'
                this.data.time_limit !== null ? this.data.time_limit = this.data.time_limit + '小时内' : ''

                this.data.image = eval(this.data.image)

                for (var i = 0; i < this.data.image.length; i++) {
                    this.data.image[i] = JSON.parse(this.data.image[i]);
                }

                if (this.data.type == 1) {
                    for (let index in this.classifyList['fabric']) {
                        console.log(this.data.sub_type, this.classifyList['fabric'][index]['id'])
                        if (this.data.sub_type == this.classifyList['fabric'][index]['id']) {
                            this.typeName = '面料 - ' + this.classifyList['fabric'][index]['name']
                        }
                    }

                } else {
                    for (let index in this.classifyList['subfabric']) {
                        console.log(this.data.sub_type, this.classifyList['subfabric'][index]['id'])
                        if (this.data.sub_type == this.classifyList['subfabric'][index]['id']) {
                            this.typeName = '辅料 - ' + this.classifyList['subfabric'][index]['name']
                        }
                    }
                }

                this.applyTime = this.data.ctime.substr(5, 11) + '发布'
            })
        },
        // 接单
        // acceptApply: function () {
        //     let id = this.$route.params.id
        //     this.$axios.get('get_agency_detail', {
        //         params: {
        //             id: this.id
        //         }
        //     }).then(res => {
        //         console.log(res)
        //     }).catch(err => {
        //         console.log(err)
        //     })
        // }
    },
    created: function () {
        this.getData();
    }
}
