
export default {
    data () {
        return {
            datas: [],
            responseList: [],
            isShow: false,
            apply_uid: ''
        }
    },
    methods: {
        goBack: function () {
            this.$router.go(-1)
        },

         // 获取反馈列表
        getData: function () {
            let id = this.$route.params.id;

            this.$axios.get('get_my_response_list', {
                headers: {
                    'crossDomain': true
                },
                params: {
                    id: this.$route.params.id
                }
            }).then(res => {
                function jsonpReturn(param) {
                    return param
                }

                let data = eval(res.data)
                if (data.code === '200') {
                    this.responseList = data.data

                    for (let i = 0; i < this.responseList.length; i++) {
                        this.apply_uid = this.responseList[i].apply_uid

                        if (this.responseList[i].content) {
                            this.responseList[i].content = JSON.parse(this.responseList[i].content)
                        }
                    }
                }
            })
        }
    },
    created: function () {
        this.getData()
    }
}
