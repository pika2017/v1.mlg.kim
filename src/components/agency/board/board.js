export default {
    data() {
        return {
            type: this.$route.params.type,
            flag: true,
            indexPage: '1',
            needPage: '0',
            colorPage: '0',
            nimePage: '0',
            feedback: '1',
            seek: '0',
            finsh: '0',
            customerDemand: '1',
            findMaterial: '1',
            finshed: '1',
            cancel: '取消',
            edit: '取消',
            contact: '编辑',
            routerId: '2',
            listBand: [],
            classifyList: {
                fabric: [
                    {
                        id: '1',
                        name: '针织'
                    },
                    {
                        id: '2',
                        name: '棉布'
                    },
                    {
                        id: '3',
                        name: '针织'
                    },
                    {
                        id: 4,
                        name: '棉布'
                    }
                ],
                subfabric: [
                    {
                        id: '1',
                        name: '纽扣'
                    },
                    {
                        id: '2',
                        name: '花边'
                    },
                    {
                        id: '3',
                        name: '色带'
                    }
                ]
            }
        }
    },
    methods: {
        goBack: function () {
            this.$router.go(-1)
        },
        changeContainFn: function (cc) {
            console.log(cc);
            if (cc == '1') {
                this.feedback = '1';
                this.seek = '0';
                this.finsh = '0';
                this.getData('1', '1')

            } else if (cc == '2') {
                this.feedback = '0';
                this.seek = '1';
                this.finsh = '0';

                this.getData('1', '2')

            } else if (cc == '3') {
                this.feedback = '0';
                this.seek = '0';
                this.finsh = '1';
                this.getData('1', '3')
            }
        },
        // 获取用户自己的找版列表
        getData(type, statues) {

            if (this.flag) {
                if (!this.flag) {
                    return
                }
                this.flag = false
                this.listBand = {}

                this.$axios.get('get_my_agency_board', {
                    headers: {
                        'crossDomain': true
                    },
                    withCredentials: true,
                    params: {
                        type: type,
                        status: statues
                    }
                }).then(res => {

                    function jsonpReturn(param) {
                        return param;
                    }

                    this.flag = true
                    let data = eval(res.data)
                    if (data.code === '200') {
                        this.listBand = data.data
                        for (let i = 0; i < this.listBand.length; i++) {
                            this.listBand[i].image = JSON.parse(this.listBand[i].image)

                            if (this.listBand[i].type === '1') {
                                for (let index in this.classifyList['fabric']) {
                                    if (this.listBand[i].sub_type === this.classifyList['fabric'][index]['id']) {
                                        this.listBand[i].type_name = '面料 - ' + this.classifyList['fabric'][index]['name']
                                    }
                                }
                            } else if (this.listBand[i].type === '2') {
                                for (let index in this.classifyList['subfabric']) {
                                    if (this.listBand[i].sub_type === this.classifyList['subfabric'][index]['id']) {
                                        this.listBand[i].type_name = '辅料 - ' + this.classifyList['subfabric'][index]['name']
                                    }
                                }
                            }

                            if (!this.listBand[i].type_name) {
                                this.listBand[i].type_name = '类别未知'
                            }

                            if (this.listBand[i].image) {
                                for (let j = 0; j < this.listBand[i].image.length; j++) {
                                    this.listBand[i].image[j] = JSON.parse(this.listBand[i].image[j])
                                    if (this.listBand[i].image[j]['path'] && !this.listBand[i].image_single) {
                                        this.listBand[i].image_single = this.listBand[i].image[j]['domain'] + this.listBand[i].image[j]['path']
                                    }
                                }
                            }

                            // 超时标志
                            if (new Date(this.listBand[i].ctime.replace("/-/g", '/')).getTime() + this.listBand[i].time_limit * 3600 * 1000 < new Date().getTime()) {
                                this.listBand[i].overtime = 1
                            }

                            // 最晚反馈时间
                            let latestTime = new Date(this.listBand[i].ctime.replace("/-/g", '/')).getTime() + parseInt(this.listBand[i].time_limit) * 3600 * 1000
                            this.listBand[i].latest_time = new Date(latestTime).getMonth() + 1 + '-' +
                            new Date(latestTime).getDate() + ' ' +
                            new Date(latestTime).getHours() + ':' +
                            new Date(latestTime).getMinutes()
                        }
                    }

                })
            }
        },

        // 取消发布
        unPublish(id) {
            this.$axios.get('cancel_agency_apply', {
                params: {
                    id: id
                }
            }).then(res => {
                console.log(res);
                function jsonpReturn(param) {
                    return param;
                }
                console.log(res.data);
                let data = eval(res.data);
                if (data.code === "200") {
                    alert('已取消');
                    this.listBand = this.listBand.filter(function (item) {
                        return item.id != id
                    })
                }
            })
        },
        //编辑
        editBand(type, id) {
            this.$router.push({ name: 'submit', params: { type: type, id: id } })
        },
        //跳转到需求详情页面
        routerLinkDetail: function (id) {
            this.$router.push(
                {
                    path: '/agency/info/' + id,
                    params: { id: id }
                }
            )
        },
        // 有反馈按钮，跳转至查看版哥反馈
        getResponeDetail: function (id) {

            this.$router.push('/agency/response/' + id)
        },
    },
    created: function () {
        this.getData('1', '1')
    },
    mounted: function () {

    }
}
