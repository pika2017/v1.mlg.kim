import plupload from '../../../../static/lib/plupload/js/plupload.min.js';

export default {
    data () {
        return {
            datas: [],
            curItemIndex: 0,
            content: [{
                imageList: [],
                name: '',
                add: '',
                phone: '',
                price: '',
                goods: ''
            }],
            imageList: [],
            isShow: false
        }
    },
    methods: {
        goBack: function () {
            this.$router.go(-1)
        },
        loadImg: function (imgs) {
            imgs += 1;
            this.imgs = imgs;
            this.upPIc = Number(this.upPIc) + Number(1);
            let that = this;
            let uploader = new plupload.Uploader({
                browse_button: 'upload',
                url: that.mlg_domain + 'Api/V1/upload_find_agency_image',
                flash_swf_url: '/static/lib/plupload/js/Moxie.swf',
                silverlight_xap_url: '/static/lib/plupload/js/Moxie.xap',
                filters: {
                    max_file_size: '10mb',
                    mime_types: [
                        { title: 'Image files', extensions: 'jpg,gif,png' },
                    ]
                },
                init: {
                    FilesAdded: function (uploader, files) {
                        uploader.start();
                    }
                }
            });
            uploader.init();
            uploader.bind('FileUploaded', function (uploader, file, res) {
                function jsonpReturn(param) {
                    return param
                }
                that.uploadProcess = 0
                let data = eval(res.response)
                let imageData = data.data
                that.content[that.curItemIndex].imageList.push(imageData)
            })

            uploader.bind('UploadProgress', function (uploader, file) {
                let percent = file.percent
                that.uploadProcess = percent
            })
        },

        // 版哥提交反馈
        submitAgencyResponse () {
            let id = this.$route.params.id

            if (!this.submitFlag) {
                this.submitFlag = 1
                this.$axios.get('submit_agency_response', {
                    params: {
                        id: id,
                        content: this.content
                    }
                }).then(res => {
                    function jsonpReturn(params) {
                        return params;
                    }

                    this.submitFlag = 0
                    let data = eval(res.data)

                    if (data.code === '200') {
                        alert('反馈成功')
                        this.$router.push({ name: 'list', params: { id: id } });
                    } else {
                        alert(data.message)
                    }
                })
            }

        }
    },
    mounted () {
        this.loadImg()
    }
}
